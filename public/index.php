<?php

// load composer dependencies
require '../vendor/autoload.php';
date_default_timezone_set('America/Mexico_City');
// Start the routing
\Src\Router::start();