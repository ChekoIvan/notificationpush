<?php
/**
 * This file contains all the routes for the project
 */
use Pecee\SimpleRouter\SimpleRouter as Router;
 
Router::group(['namespace' => '\Src\Controllers', 'exceptionHandler' => \Src\Handlers\CustomExceptionHandler::class], function () {

    Router::get('/', function() {
        return 'Index page';
    });
    
    Router::get('/intervalo', 'IntervalController@index');
    
	Router::get('/query', 'QueryController@index');

    //Router::get('/run-notification-process-test', 'NotificationController@run');
    
    Router::get('/run-notification-process', 'NotificationController@runNotificationProcess');

  

});