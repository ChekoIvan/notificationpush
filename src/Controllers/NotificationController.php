<?php
namespace Src\Controllers;
use Src\Servicios\SendPushNotification;
use Src\Clases\Query;
use Src\Clases\UTF8String;
use Src\Clases\ConnectAPI;
use ExponentPhpSDK\Exceptions\ExpoException;
use ExponentPhpSDK\Exceptions\UnexpectedResponseException;
use \DateTime as DateTime;

class NotificationController
{
	// public function runNotificationBroadcastProcess(){
	// 	$data = $this->getQueryData();
		
    //     foreach ($data as $key => $item) {
	// 		$fecha = new DateTime($item->start);
	// 		$now = new DateTime();
	// 		//error_log($now->format("Y-m-d H:i:s") . "\t::Trying to send notification::".$item->token);
	// 		$notificationBody = [
	// 			'body'=>'Cita con: ' . $item->title . ' a las ' . $fecha->format("H:i") . ', lugar: ' . $item->place,
	// 			'data'=>$item
	// 		];
    //         SendPushNotification::sendChannelNotification($item->token, $notificationBody);
    //     }
	// }
	public function getQueryData(){
		$query = new Query();
		$data = $query->getData();
		return $data;
	}
		 private static function verificarRespuesta(array $expoResponse):int{
				 $conteo = 0;
				 foreach ($expoResponse as $key => $value) {
						 ($value['status']=='error') && $conteo++;
				 }
				 return $conteo;
		 }
 
	 public function runNotificationProcess(){
				 $data = $this->getQueryData();
				 $resultSet=[];
		 foreach ($data as $key => $item) {
						 $fecha = new DateTime($item['start']);
						 //$now = new DateTime();
						 //error_log($now->format("Y-m-d H:i:s") . "\t::Trying to send notification::".$item->token);
						 $notificationBody = [
								 'body'=>UTF8String::toUTF8(utf8_decode('Cita con: ' . $item['title'] . ' a las ' . $fecha->format("H:i") . ', lugar: ' . $item['place'])),
								 'data'=>json_encode($item)
						 ];
						 $serviceResponse=[];
				 try {
								 $expoTokens = preg_split('/,/',$item['tokens']);
								 //error_log(count($expoTokens));
								 $serviceResponse = SendPushNotification::sendSingleNotification($expoTokens, $notificationBody);
								 $conteo = NotificationController::verificarRespuesta($serviceResponse);
								 if($conteo<count($expoTokens)){
										 error_log('Llamada a API');
										 //aqui se llama a la API con item['idcita'] y $item['apikey']
										 $serviceResponse['notifyAppointment'] = ConnectAPI::sendNotifiedAppointment($item['apikey'],$item['idcita']);
										 $serviceResponse['createNotification'] = ConnectAPI::sentCreatedNotification($item['apikey'],$item['idcita']);
								 }
 
						 }
				 catch (UnexpectedResponseException $ure){
								 $serviceResponse['status'] = 'error';
								 $serviceResponse['info'] = "Problemas al realizar la petición";
								 $serviceResponse['details'] = $ure->getMessage();
				 }
				  catch (ExpoException $ee){
								 $serviceResponse['status'] = 'error';
								 $serviceResponse['info'] = "Respuesta de error de API de Expo";
								 $serviceResponse['details'] =$ee->getMessage();
				 }
						 array_push($resultSet, $serviceResponse);
		 }
				 return response()->json($resultSet);
		 }
 }