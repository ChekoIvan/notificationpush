<?php
namespace Src\Controllers;
use Src\Clases\Query;
use Src\Clases\Intervalo;

class QueryController{
   
    public function index(){
        $Query = new Query();
        return response()->json($Query->getData());
    }

}