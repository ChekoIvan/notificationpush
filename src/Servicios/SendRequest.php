<?php
date_default_timezone_set('America/Mexico_City');

$ch = curl_init('http://localhost:8000/run-notification-process');

curl_setopt($ch, CURLOPT_HEADER, 0);
curl_exec($ch);
curl_close($ch);

$fecha = new \DateTime();
echo $fecha->format("Y-m-d H:i:s") . "\tCall to notification process\n";

?>