<?php
namespace Src\Servicios;
use Src\Clases\ExpoSingle;

class SendPushNotification 
{
    
    public static function sendChannelNotification($name, $notificationBody):string{
        $channelName = $name;
        
        // You can quickly bootup an expo instance
        $expo = \ExponentPhpSDK\Expo::normalSetup();
                
        // Build the notification data
        $notification = ['title'=>'Apolofy','body' => $notificationBody['body'], 'data'=> json_encode($notificationBody['data'])];
      
        // Notify an interest with a notification
        $expo->notify($channelName, $notification);
        return 'OK';
    }
    public static function sendSingleNotification($token, $notificationBody):array{
        
        $expo = new ExpoSingle();
                
        // Build the notification data
        $notification = ['title'=>'Apolofy','body' => $notificationBody['body'], 'data'=> $notificationBody['data']];
      
        // Notify an interest with a notification
        $response = $expo->notify($token, $notification);       
        return $response;
    }
}

 