<?php
namespace Src\Clases;
use \DateTime as DateTime;

class ConnectAPI
{
    const API_URL_NOTIFY_APPOINTMENT =  'https://cloudapolofy.com/maa/mvc/Agenda/api/notifyAppointment.php';
    const API_URL_CREATE_NOTIFICATION = 'https://cloudapolofy.com/maa/mvc/Notificacion/api/createNotification.php';
    const API_URL_GET_APPOINTMENTS =    'https://cloudapolofy.com/maa/mvc/Agenda/api/getListAppointmentPush.php?key=tWMmvXswvVfgXVh';

    public static function sendNotifiedAppointment($apikey, $idcita) {
        $ch = ConnectAPI::prepareCurl(true,self::API_URL_NOTIFY_APPOINTMENT.'?apikey='.$apikey.'&idcita='.$idcita);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }
    public static function sentCreatedNotification($apikey, $idcita){
        $ch = ConnectAPI::prepareCurl(true,self::API_URL_CREATE_NOTIFICATION.'?apikey='.$apikey.'&idcita='.$idcita);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    public static function getAppointmentsData(){
        $ch = ConnectAPI::prepareCurl(true,self::API_URL_GET_APPOINTMENTS);
        $apiResponse = curl_exec($ch);

        $response = [
            'body' => $apiResponse,
            'status_code' => curl_getinfo($ch, CURLINFO_HTTP_CODE)
        ];
        $responseData = json_decode($response['body'],true) ?? null;
        curl_close($ch);
        return $responseData;
    }

    private static function prepareCurl($isPOST, $URL)
    {
        $ch = curl_init();
        if (!$ch) {
            throw new ExpoException('Could not initialise cURL!');
        }

        curl_setopt($ch, CURLOPT_URL, $URL);

        // Set cURL opts

        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'accept: application/json',
            'content-type: application/json',
        ]);
        curl_setopt($ch, CURLOPT_POST, $isPOST);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        return $ch;
    }
}
