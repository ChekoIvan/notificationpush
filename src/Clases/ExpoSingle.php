<?php
namespace Src\Clases;
use ExponentPhpSDK\Exceptions\ExpoException;
use ExponentPhpSDK\Exceptions\UnexpectedResponseException;

class ExpoSingle  {   

    /**
     * The Expo Api Url that will receive the requests
     */
    const EXPO_API_URL = 'https://exp.host/--/api/v2/push/send';

    /**
     * cURL handler
     *
     * @var null|resource
     */
    private $ch = null;

 
    /**
     * Expo constructor.
     *
     * @param ExpoRegistrar $expoRegistrar
     */
    public function __construct( )
    {
    }

    /**
     * Send a notification via the Expo Push Notifications Api.
     *
     * @param $interests
     * @param array $data
     * @param bool $debug
     *
     * @throws ExpoException
     * @throws UnexpectedResponseException
     *
     * @return array|bool
     */
    public function notify(array $expoTokens, array $data, $debug = false)
    {
        $postData = [];
        // if (empty($expoTokens)) {
        //     return ["Solicitud vacía"];
        // }
        foreach ($expoTokens as $token) {
            $notification = new \ArrayObject($data);
            $notification['to'] = $token;
            array_push($postData, $notification);
        }

        //error_log("request::".json_encode($postData));
        $ch = $this->prepareCurl();

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));
        
        $response = $this->executeCurl($ch);
        // If the notification failed completely, throw an exception with the details
        if (!$debug && $this->failedCompletely($response, $expoTokens)) {
            throw ExpoException::failedCompletelyException($response);
        }

        return $response;
    }

    private function failedCompletely(array $response, array $interests)
    {
        $numberOfInterests = count($interests);
        $numberOfFailures = 0;

        foreach ($response as $item) {
            if ($item['status'] === 'error') {
                $numberOfFailures++;
            }
        }

        return $numberOfFailures === $numberOfInterests;
    }

    /**
     * Sets the request url and headers
     *
     * @throws ExpoException
     *
     * @return null|resource
     */
    private function prepareCurl()
    {
        $ch = $this->getCurl();

        // Set cURL opts
        curl_setopt($ch, CURLOPT_URL, self::EXPO_API_URL);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'accept: application/json',
            'content-type: application/json',
        ]);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        return $ch;
    }

    /**
     * Get the cURL resource
     *
     * @throws ExpoException
     *
     * @return null|resource
     */
    public function getCurl()
    {
        // Create or reuse existing cURL handle
        $this->ch = $this->ch ?? curl_init();

        // Throw exception if the cURL handle failed
        if (!$this->ch) {
            throw new ExpoException('Could not initialise cURL!');
        }

        return $this->ch;
    }

    /**
     * Executes cURL and captures the response
     *
     * @param $ch
     *
     * @throws UnexpectedResponseException
     *
     * @return array
     */
    private function executeCurl($ch)
    {
        $response = [
            'body' => curl_exec($ch),
            'status_code' => curl_getinfo($ch, CURLINFO_HTTP_CODE)
        ];
        //error_log(json_encode($response));

        $responseData = json_decode($response['body'], true)['data'] ?? null;

        if (! is_array($responseData)) {
            throw new UnexpectedResponseException();
        }

        return $responseData;
    }
}
