<?php
namespace Src\Clases;
use \DateTime as DateTime;
use Src\Clases\ConnectAPI;
class Query 
{
    private $data;

    public function __construct(){
        // $nombre_fichero = "../src/Data/citas.json";
        // $gestor = fopen($nombre_fichero, "r");
        // $notificaciones  = fread($gestor, filesize($nombre_fichero));
        // fclose($gestor);
        // $this->data = json_decode($notificaciones,true);
        $this->data = ConnectAPI::getAppointmentsData();

    }
    private function isNotifiable($item) :bool {
        $intervalo = $item['intervalo'];
        $fechaCita = new DateTime($item['start']);
        
        // Ajustes para fecha especifica
        // $fechaActual = new DateTime('2020-02-26');
        // $fechaLimite = new DateTime('2020-02-27');
         //$fechaLimite->modify("+{$intervalo} minutes");
        
        $fechaActual = new DateTime();
        $fechaLimite = new DateTime("+{$intervalo} minutes");
                
        //error_log("FechaActual::".$fechaActual->format('Y-m-d H:i:s') ."-FechaLimite::".$fechaLimite->format('Y-m-d H:i:s'));
        return $fechaCita > $fechaActual && $fechaCita < $fechaLimite;
    }
    public function getData(){
        $dataNotifiable = array_filter($this->data, array($this,"isNotifiable"));
        return $dataNotifiable;
        //return $this->data;
    }

}
